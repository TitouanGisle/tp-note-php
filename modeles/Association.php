<?php

namespace LP\Modeles;
require_once(__DIR__.'/Structure.php');
use LP\Modeles\Structure;

class Association extends Structure {
    private $nb_donateurs;

    public function __construct(String $nom, String $rue, String $code_postal, String $ville, int $nb_donateurs = 0, int $id = null) {
        parent::__construct($nom, $rue, $code_postal, $ville, $id);
        $this->setNbDonateurs($nb_donateurs);
    }

    public function setNbDonateurs(int $nb_donateurs) { $this->nb_donateurs = $nb_donateurs; return $this; }

    public function getNbDonateurs() { return $this->nb_donateurs; }
}
