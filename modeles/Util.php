<?php

namespace LP\Modeles;

require_once(__DIR__."/Structure.php");
require_once(__DIR__."/Entreprise.php");
require_once(__DIR__."/Association.php");

use LP\Modeles\{Structure, Entreprise, Association};


class Util {

    /**
    * Construit et renvoie une entreprise, ou une association
    *
    * @return Entreprise|Association
    */
    public static function buildStructure(String $nom, String $rue, String $cp, String $ville, int $gens, bool $is_asso, int $id = null) {

        if($is_asso) {
            return new Association($nom, $rue, $cp, $ville, $gens, $id);
        }

        return new Entreprise($nom, $rue, $cp, $ville, $gens, $id);
    }
}
