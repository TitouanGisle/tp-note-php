<?php

namespace LP\Modeles;

require_once(__DIR__.'/../database/TPPDO.php');
use LP\Database\TPPDO;

class Secteur {

    private $id;
    private $libelle;

    /**
     * Constructeur
     */
    public function __construct(String $libelle, int $id=null){
        $this->libelle = $libelle;
        $this->id = $id;
    }

    /**
     * Getters et setters
     */

    public function getLibelle(){
        return $this->libelle;
    }

    public function setLibelle(String $libelle){
        $this->libelle = $libelle;
    }

    public function getId(){
        return $this->id;
    }

}
