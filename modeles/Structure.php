<?php

namespace LP\Modeles;

require_once(__DIR__.'/../database/TPPDO.php');
use LP\Database\TPPDO;
require_once(__DIR__.'/Util.php');
use LP\Modeles\Util;

abstract class Structure {
    private $id;
    private $nom;
    private $rue;
    private $code_postal;
    private $ville;

    /**
     * Constructeur
     */
    public function __construct(String $nom, String $rue, String $code_postal, String $ville, int $id=null) {
        $this->setNom($nom)
            ->setRue($rue)
            ->setCodePostal($code_postal)
            ->setVille($ville);
        $this->id = $id;
    }

    /**
     * Getters et setters
     */
    public function getNom() { return $this->nom; }

    public function setNom(String $nom) { $this->nom = $nom; return $this; }

    public function getRue() { return $this->rue; }

    public function setRue(String $rue) { $this->rue = $rue; return $this; }

    public function getCodePostal() { return $this->code_postal; }

    public function setCodePostal(String $code_postal) { $this->code_postal = $code_postal; return $this; }

    public function getVille() { return $this->ville; }

    public function setVille(String $ville) { $this->ville = $ville; return $this; }

    /**
    * Indique si l'objet est une Association
    */
    public function isAsso() {
        return $this instanceof Association;
    }

    public function getId() { return $this->id; }

    public function toArray() {
        return ['id' => $this->getID(),
                'nom' => $this->getNom(),
                'rue' => $this->getRue(),
                'code_postal' => $this->getCodePostal(),
                'ville' => $this->getVille(),
                'nb_actionnaires' => $this->isAsso() ? null : $this->getNbActionnaires(),
                'nb_donateurs' => $this->isAsso() ? $this->getNbDonateurs() : null
            ];
    }

}
