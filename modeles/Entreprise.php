<?php

namespace LP\Modeles;
require_once(__DIR__.'/Structure.php');
use LP\Modeles\Structure;

class Entreprise extends Structure {
    private $nb_actionnaires;

    public function __construct(String $nom, String $rue, String $code_postal, String $ville, int $nb_actionnaires = 0, int $id = null) {
        parent::__construct($nom, $rue, $code_postal, $ville, $id);
        $this->setNbActionnaires($nb_actionnaires);
    }

    public function setNbActionnaires($nb_actionnaires) { $this->nb_actionnaires = $nb_actionnaires; return $this; }

    public function getNbActionnaires() { return $this->nb_actionnaires; }
}
