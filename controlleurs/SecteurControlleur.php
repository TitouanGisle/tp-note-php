<?php

namespace LP\Controlleurs;

require_once(__DIR__.'/../modeles/Secteur.php');
use LP\Modeles\Secteur;

require_once(__DIR__.'/../database/SecteurDAO.php');
use LP\DAO\SecteurDAO;

require_once(__DIR__.'/../database/LinkDAO.php');
use LP\DAO\LinkDAO;

class SecteurControlleur {

    /**
    * Affiche la liste des secteurs
    */
    public function viewSecteurs() {

        $secteurs = SecteurDAO::all();

        require(__DIR__.'/../vues/viewSecteurs.php');
    }

    /**
    * Stock en BDD le secteur créé dans le formulaire
    */
    public function addSecteur() {

        $secteur = new Secteur($_POST['libelle']);

        SecteurDAO::store($secteur);

        header("Location: index.php?action=viewSecteurs");
    }

    /**
    * Supprime de la BDD le secteur dont l'id est passée en paramètre
    * @param int $id
    */
    public function deleteSecteur(int $id) {

        SecteurDAO::delete($id);

        header("Location: index.php?action=viewSecteurs");
    }

    /**
    * Supprime la relation entre un secteur et une structure
    * @param int $id_secteur
    * @param int $id_structure
    */
    public function unlinkSecteurFromStructure(int $id_secteur, int $id_structure) {

        LinkDAO::unlinkSecteurFromStructure($id_secteur, $id_structure);

        header("Location: index.php?action=viewStructure&id={$id_structure}");
    }

    /**
    * Ajoute une relation entre un secteur et une structure
    * @param int $id_secteur
    * @param int $id_structure
    */
    public function linkSecteurToStructure(int $id_secteur, int $id_structure) {

        LinkDAO::linkSecteurToStructure($id_secteur, $id_structure);

        header("Location: index.php?action=viewStructure&id={$id_structure}");
    }
}
