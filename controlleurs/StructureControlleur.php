<?php

namespace LP\Controlleurs;

require_once(__DIR__.'/../modeles/Structure.php');
use LP\Modeles\Structure;

require_once(__DIR__.'/../database/StructureDAO.php');
use LP\DAO\StructureDAO;

require_once(__DIR__.'/../database/LinkDAO.php');
use LP\DAO\LinkDAO;

require_once(__DIR__.'/../modeles/Util.php');
use LP\Modeles\Util;


class StructureControlleur {

    /**
    * Affiche la structure dont l'id est passée en paramètre, et ses secteurs
    * @param int $id
    */
    public function viewStructure(int $id) {

        $structure = StructureDAO::withId($id);

        $secteurs = LinkDAO::getSecteursFor($structure->getId());

        $secteurs_restants = LinkDAO::getOtherSecteursFor($structure->getId());

        require(__DIR__.'/../vues/viewStructure.php');
    }

    /**
    * Affiche la liste des Structures
    */
    public function viewStructures() {

        $structures = StructureDAO::all();

        $saved_structure = json_decode($_SESSION['saved_structure'] ?? null);

        require(__DIR__.'/../vues/viewStructures.php');
    }

    /**
    * Traite une requête POST pour ajouter une structure
    */
    public function addStructure() {

        $is_asso = isset($_POST['asso']);

        $gens = $is_asso ? $_POST['donat'] : $_POST['actio'];

        $structure = Util::buildStructure($_POST['nom'], $_POST['rue'], $_POST['cp'], $_POST['ville'], $gens, $is_asso);

        StructureDAO::store($structure);

        $_SESSION['saved_structure'] = json_encode($structure->toArray());

        header("Location: index.php?action=viewStructures");
    }

    /**
    * Modifie une structure
    */
    public function editStructure() {

        $is_asso = isset($_POST['asso']);

        $gens = $is_asso ? $_POST['donat'] : $_POST['actio'];

        $structure = Util::buildStructure($_POST['nom'], $_POST['rue'], $_POST['cp'], $_POST['ville'], $gens, $is_asso, $_GET['id']);

        StructureDAO::update($structure);

        header("Location: index.php?action=viewStructures");
    }

    /**
    * Supprime une structure
    */
    public function deleteStructure() {

        StructureDAO::delete($_GET['id']);

        header("Location: index.php?action=viewStructures");
    }
}
