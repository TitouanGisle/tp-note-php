<?php
session_start();

require_once(__DIR__.'\controlleurs\StructureControlleur.php');
require_once(__DIR__.'\controlleurs\SecteurControlleur.php');
use LP\Controlleurs\StructureControlleur;
use LP\Controlleurs\SecteurControlleur;

try {
   if (isset($_GET["action"])) {
       if (stripos($_GET["action"], "structure")) {
           $controlleur = new StructureControlleur();
           switch ($_GET["action"]) {
               case "viewStructure":
                   if (isset($_GET["id"]) && $_GET["id"] > 0) {
                       $structure = $controlleur->viewStructure($_GET["id"]);
                   } else {
                       $error = "Erreur : mauvais identifiant<br/>";
                   }
                   break;
               case "viewStructures":
                   $structures = $controlleur->viewStructures();
                   break;
               case "addStructure":
                   if (isset($_POST["nom"], $_POST["rue"], $_POST["cp"], $_POST["ville"])) {
                       $controlleur->addStructure();
                   } else {
                       $error = "Paramètre(s) manquant(s)<br/>";
                   }
                   break;
               case "editStructure":
                   if (isset($_POST["nom"], $_POST["rue"], $_POST["cp"], $_POST["ville"])) {
                       $controlleur->editStructure();
                   } else {
                       $error = "Paramètre(s) manquant(s)<br/>";
                   }
                   break;
               case "deleteStructure":
                   if (isset($_GET["id"]) && $_GET["id"]>0) {
                       $controlleur->deleteStructure();
                   } else {
                       $error = "Paramètre(s) manquant(s)<br/>";
                   }
                   break;
               default :
                   $error = "Erreur : action non reconnue<br/>";
                   break;
           }
       } elseif (stripos($_GET["action"], "secteur")) {
           $controlleur = new SecteurControlleur();
           switch ($_GET["action"]) {
               case "viewSecteur":
                   if (isset($_GET["id"]) && $_GET["id"] > 0) {
                       $secteur = $controlleur->viewSecteur($_GET["id"]);
                   } else {
                       $error = "Erreur : mauvais identifiant<br/>";
                   }
                   break;
               case "viewSecteurs":
                   $secteurs = $controlleur->viewSecteurs();
                   break;
               case "addSecteur":
                   if (isset($_POST["libelle"])) {
                       $controlleur->addSecteur();
                   } else {
                       $error = "Erreur de paramètres<br/>";
                   }
                   break;
                case "deleteSecteur":
                    if(isset($_GET["id"])){
                        $controlleur->deleteSecteur($_GET["id"]);
                    } else {
                        $error = "Erreur de suppression<br/>";
                    }
                    break;
                case "unlinkSecteur":
                    if(isset($_GET["id_secteur"]) && isset($_GET["id_structure"])) {
                        $controlleur->unlinkSecteurFromStructure($_GET["id_secteur"], $_GET["id_structure"]);
                    } else {
                        $error = "Erreur de liaison structure-secteur";
                    }
                    break;
                case "linkSecteur":
                    if(isset($_GET["id_structure"]) && isset($_POST["select_secteur"])) {
                        $controlleur->linkSecteurToStructure($_POST["select_secteur"], $_GET["id_structure"]);
                    } else {
                        $error = "Erreur de liaison structure-secteur";
                    }
                    break;
               default :
                   $error = "Erreur : action non reconnue<br/>";
                   break;
           }
       } else {
           $error = "Erreur : action non reconnue<br/>";
       }
   } else {
       ?>
       <a href="index.php?action=viewStructures">Liste des structures</a><br/>
       <a href="index.php?action=viewSecteurs">Liste des secteurs</a>
       <?php
   }
}
catch (Exception $ex) {
   $error="Error ".$ex->getCode()." : ".$ex->getMessage()."<br/>".str_replace("\n","<br/>",$ex->getTraceAsString());
}
if (isset($error)) {
   require(__DIR__.'\vues\error.php');
}
