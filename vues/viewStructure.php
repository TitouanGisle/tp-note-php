<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $structure->getNom() ?></title>
</head>
<a href="index.php?action=viewStructures">retour</a>

<form method="POST" action="index.php?action=editStructure&id=<?= $structure->getId() ?>">
    <table>
        <tbody>
            <tr>
                <td><label for="nom">Nom :</label></td>
                <td><input type="text" name="nom" placeholder="Nom :" required value="<?= $structure->getNom() ?>"></td>
            </tr>
            <tr>
                <td><label for="nom">Rue :</label></td>
                <td><input type="text" name="rue" placeholder="Rue :" required value="<?= $structure->getRue() ?>"></td>
            </tr>
            <tr>
                <td><label for="nom">Code Postal :</label></td>
                <td><input type="text" name="cp" placeholder="Code postal :" required value="<?= $structure->getCodePostal() ?>"></td>
            </tr>
            <tr>
                <td><label for="nom">Ville :</label></td>
                <td><input type="text" name="ville" placeholder="Ville :" required value="<?= $structure->getVille() ?>"></td>
            </tr>
            <tr>
                <td><label for="nom">Association :</label></td>
                <td><input type="checkbox" name="asso" value="1" id="asso-check" <?= $structure->isAsso() ? "checked" : null ?>></td>
            </tr>
            <tr>
                <td><label for="nom">Nombre de donateurs :</label></td>
                <td><input type="number" name="donat" placeholder="Donateurs :" id="input-donat" value="<?= $structure->isAsso() ? $structure->getNbDonateurs() : null ?>" required  <?= $structure->isAsso() ? null : "disabled" ?>></td>
            </tr>
            <tr>
                <td><label for="nom">Nombre d'actionnaires :</label></td>
                <td><input type="number" name="actio" placeholder="Actionnaires :" id="input-actio" value="<?= $structure->isAsso() ? null : $structure->getNbActionnaires() ?>" required  <?= $structure->isAsso() ? "disabled" : null ?>></td>
            </tr>
        </tbody>
    </table>
    <button type="submit">Valider modification</button>
</form>
<br>
<h4>Secteurs couverts par <?=$structure->getNom() ?> : </h4>
<ul>
    <?php if(empty($secteurs) || $secteurs==null) {
            echo "<p>Aucun</p>";
        } else {
            foreach ($secteurs as $id => $secteur) {
                echo "<li>{$secteur->getLibelle()} <a href='index.php?action=unlinkSecteur&id_secteur={$id}&id_structure={$structure->getId()}'>(x)</a></li>";
            }
        } ?>
</ul>
<?php if(!empty($secteurs_restants)) : ?>
<p>Ajouter un secteur : </p>
<form action="index.php?action=linkSecteur&id_structure=<?= $structure->getId() ?>" method="post">
    <select name="select_secteur">
        <?php foreach ($secteurs_restants as $secteur) {
            echo "<option value='{$secteur->getId()}'>{$secteur->getLibelle()}</option>";
        } ?>
    </select>
    <button type="submit" name="button">Ajouter</button>
</form>
<?php endif; ?>

<script type="text/javascript">
    // Juste pour gérer l'affichage des donateurs/actionaires du formulaire,
    // en fonction de la checkbox Association
    window.onload = function () {
        let checkbox = document.getElementById("asso-check");
        checkbox.addEventListener('change', function(event) {
            if (event.target.checked) {
                document.getElementById('input-donat').disabled = false;
                document.getElementById('input-actio').disabled = true;
            } else {
                document.getElementById('input-donat').disabled = true;
                document.getElementById('input-actio').disabled = false;
            }
        });
    };
</script>
