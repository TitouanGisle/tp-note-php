<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Structures</title>
</head>
<style>
    .list {
        border-collapse : collapse;
    }
    .list th {
        background-color : lightgrey;
        border : 1px solid grey;
        padding : 0.5em;
    }
    .list td {
        border : 1px solid grey;
        padding : 0.5em;
    }
</style>
<body>
    <h4> Liste des structures </h4>
    <a href="index.php">retour</a>

    <table class='list'>
        <tbody>
            <tr><th>Nom</th><th>Rue</th><th>Code Postal</th><th>Ville</th><th>Association ?</th><th>Nombre de donateurs</th><th>Nombre d'actionnaires</th><th></th></tr>
                <?php foreach ($structures as $structure) : ?>
                    <tr>
                        <td>
                            <a href="index.php?action=viewStructure&id=<?=$structure->getId()?>">
                                <?= $structure->getNom() ?>
                            </a>
                        </td>
                        <td><?= $structure->getRue() ?></td>
                        <td><?= $structure->getCodePostal() ?></td>
                        <td><?= $structure->getVille() ?></td>
                        <td><?= $structure->isAsso() ? 'Oui' : 'Non' ?></td>
                        <td><?= $structure->isAsso() ? $structure->getNbDonateurs() : null; ?></td>
                        <td><?= $structure->isAsso() ? null :  $structure->getNbActionnaires() ; ?></td>
                        <td><a href="index.php?action=deleteStructure&id=<?=$structure->getId()?>"><button>Supprimer</button></a></td>
                    </tr>
                <?php endforeach;?>

                <tr><th colspan='8'>Ajouter une structure</th></tr>
                <form method="POST" action="index.php?action=addStructure">
                    <tr>
                        <td><input type="text" name="nom" placeholder="Nom :" required value="<?= $saved_structure->nom ?? '' ?>"></td>
                        <td><input type="text" name="rue" placeholder="Rue :" required value="<?= $saved_structure->rue  ?? '' ?>"></td>
                        <td><input type="text" name="cp" placeholder="Code postal :" required value="<?= $saved_structure->code_postal  ?? '' ?>"></td>
                        <td><input type="text" name="ville" placeholder="Ville :" required value="<?= $saved_structure->ville  ?? '' ?>"></td>
                        <td><input type="checkbox" name="asso" value="1" id="asso-check" <?= isset($saved_structure->nb_donateurs) ? 'checked=true' : '' ?> > </td>
                        <td><input type="number" name="donat" placeholder="Donateurs :"
                            id="input-donat" style="display:none" value="<?= $saved_structure->nb_donateurs ?? '0' ?>"></td>
                        <td><input type="number" name="actio" placeholder="Actionnaires :"
                            id="input-actio" value="<?= $saved_structure->nb_actionnaires ?? '0' ?>"></td>
                        <td></td>
                    </tr>
                    <tr><td colspan='8'><button type="submit">Valider</button></td></tr>
                </form>
        </tbody>
    </table>
</body>
<script type="text/javascript">
    // Juste pour g�rer l'affichage des donateurs/actionaires du formulaire,
    // en fonction de la checkbox Association
    window.onload = function () {
        let checkbox = document.getElementById("asso-check");
        checkbox.addEventListener('change', function(event) {
            if (event.target.checked) {
                document.getElementById('input-donat').setAttribute('style', 'display:block');
                document.getElementById('input-actio').setAttribute('style', 'display:none');
            } else {
                document.getElementById('input-donat').setAttribute('style', 'display:none')
                document.getElementById('input-actio').setAttribute('style', 'display:block');
            }
        });
    };
</script>
</html>
