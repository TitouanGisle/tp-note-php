<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Secteurs</title>
</head>
<style>
    .list {
        border-collapse : collapse;
    }
    .list th {
        background-color : lightgrey;
        border : 1px solid grey;
        padding : 0.5em;
    }
    .list td {
        border : 1px solid grey;
        padding : 0.5em;
    }
</style>
<body>
    <h4> Liste des secteurs </h4>
    <a href="index.php">retour</a>

    <table class='list'>
        <tbody>
            <tr><th>Libelle</th><th>Suppression</th></tr>
                <?php foreach ($secteurs as $secteur) : ?>
                    <tr>
                        <td><?= $secteur->getLibelle() ?></td>
                        <td>
                        <a href="index.php?action=deleteSecteur&id=<?= $secteur->getID()?>"><button>Supprimer</button></a></td>
                        <!--
                            <form method="POST" action="index.php?action=deleteSecteur">
                            <input type="hidden" name="id_secteur" value="<?= $secteur->getID()?>">
                                <button type="submit">Supprimer</button>
                            </form>
                        </td> -->
                        <!-- Bouton supprimer -->
                    </tr>
                <?php endforeach;?>

                <tr><th>Ajouter un secteur</th></tr>
                <form method="POST" action="index.php?action=addSecteur">
                    <tr>
                        <td><input type="text" name="libelle" placeholder="libelle" required></td>
                    </tr>
                    <tr><td><button type="submit">Valider</button></td></tr>
                </form>
        </tbody>
    </table>
</body>
</html>
