<?php

namespace LP\Database;

use PDO;

/**
* Classe qui étend la classe PDO et y ajoute les paramètres du .ini
*/
class TPPDO extends PDO {

    public function __construct() {
        //récupération des paramètre de connexion à la bdd
        if (!$settings = parse_ini_file('database.ini', TRUE)) {
            throw new exception("Impossible d'ouvrir le fichier de paramètres '/database/database.ini'.");
        }
        $server = $settings['database']['server'];
        $db = $settings['database']['db'];
        $user = $settings['database']['user'];
        $password = $settings['database']['password'];

        //Création du PDO
        parent::__construct("mysql:host=$server;dbname=$db", $user, $password);

        //Paramètrage PDO
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}
