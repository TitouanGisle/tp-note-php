<?php

namespace LP\DAO;

require_once(__DIR__.'/../modeles/Structure.php');
use LP\Modeles\Structure;

require_once(__DIR__.'/../modeles/Secteur.php');
use LP\Modeles\Secteur;

require_once(__DIR__.'/../modeles/Util.php');
use LP\Modeles\Util;

require_once(__DIR__.'/TPPDO.php');
use LP\Database\TPPDO;



class LinkDAO {

    /**
     * Renvoie les secteurs associés à la structure passée en paramètre
     * @param int $id_structure
     * @return Array<Secteur>
     */
    public static function getSecteursFor(int $id_structure) {
        try {
            $pdo = new TPPDO();

            $statement = $pdo->prepare(
                "SELECT secteur.*
                FROM secteur, structure, secteurs_structures
                WHERE secteur.id = secteurs_structures.id_secteur
                AND structure.id = secteurs_structures.id_structure
                AND structure.id = :id");

            $statement->execute(['id' => $id_structure]);

            $secteurs = $statement->fetchAll();

            $secteur_objets = [];

            //Transformation des données extraites en objets
            foreach ($secteurs as $secteur) {
                $secteur_objets[$secteur['ID']] = new Secteur($secteur['LIBELLE'], $secteur['ID']);
            }

            return $secteur_objets;

        } catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de la récupération des secteurs de la structure : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }

        return null;
    }

    /**
    * Lie un secteur à une structure
    * @param int $id_secteur
    * @param int $id_structure
    * @return bool
    */
    public static function linkSecteurToStructure(int $id_secteur, int $id_structure) {
        try {
            $pdo = new TPPDO();

            $statement = $pdo->prepare(
                "INSERT INTO secteurs_structures(id_secteur, id_structure)
                VALUES (:id_secteur, :id_structure)");

            $statement->execute(['id_secteur' => (int) $id_secteur, 'id_structure' => (int) $id_structure]);

            return true;

        } catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de l'ajout d'un secteur à une structure : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }

        return false;
    }

    /**
    * Supprime la relation entre un secteur et une structure
    * @param int $id_secteur
    * @param int $id_structure
    * @return bool
    */
    public static function unlinkSecteurFromStructure(int $id_secteur, int $id_structure) {
        try {
            $pdo = new TPPDO();

            $statement = $pdo->prepare(
                "DELETE FROM secteurs_structures
                WHERE id_secteur = :id_secteur
                AND id_structure = :id_structure");

            $statement->execute(['id_secteur' => $id_secteur, 'id_structure' => $id_structure]);

            return true;

        } catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de l'ajout d'un secteur à une structure : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }

        return false;
    }

    /**
    * Retourne les secteurs non couverts par la structure passée en paramètre
    * @param int $id_structure
    * @return Array<Secteur>
    */
    public static function getOtherSecteursFor(int $id_structure) {
        try {
            $pdo = new TPPDO();

            $statement = $pdo->prepare(
                "SELECT secteur.*
                FROM secteur, structure, secteurs_structures
                WHERE secteur.id NOT IN (
                    SELECT id_secteur
                    FROM secteurs_structures
                    WHERE id_structure = :id
                ) ");

            $statement->execute(['id' => $id_structure]);

            $secteurs = $statement->fetchAll();

            $secteur_objects = [];

            foreach ($secteurs as $secteur) {
                $secteur_objets[$secteur['ID']] = new Secteur($secteur['LIBELLE'], $secteur['ID']);
            }

            return $secteur_objets;

        } catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de la récupération des secteurs étrangers à la structure : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }

        return null;
    }
}
