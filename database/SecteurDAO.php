<?php

namespace LP\DAO;

require_once(__DIR__.'/../modeles/Secteur.php');
use LP\Modeles\Secteur;

require_once(__DIR__.'/TPPDO.php');
use LP\Database\TPPDO;



class SecteurDAO {

    /**
     * Renvoie tous les secteurs présents dans la BDD
     * @return Array<Secteur>
     */
    public static function all(){
        try{
            $pdo = new TPPDO();
            $statement = $pdo->prepare("SELECT * FROM secteur");
            $statement->execute();
            $secteurs = $statement->fetchAll();

            $secteur_objects = [];
            foreach($secteurs as $secteur){
                $secteur_objects[] = new Secteur($secteur['LIBELLE'], $secteur['ID']);
            }
            return $secteur_objects;
        }

        catch(PDOException $e){
            echo "Erreur ".$e->getCode()." lors de la récupération des Secteurs : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }
        return null;
    }

    /**
     * Renvoie le secteur avec l'id passé en paramètre, ou bien null
     * @param int $id
     * @return Secteur|null
     */
    public static function withId(int $id){
        try{
            $pdo = new TPPDO();
            $statement = $pdo->prepare("SELECT * FROM secteur WHERE id = :id");
            $statement->execute(["id" => $id]);
            $secteur = $statement->fetchAll();
            $secteur_object = new Secteur($libelle["LIBELLE"]);

            return $secteur_object;
        }
        catch(PDOException $e){
            echo "Erreur ".$e->getCode()." lors de la récupération d'un Secteur : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }
        return null;
    }

    /**
     * Enregistre en BDD le secteur passé en paramètre
     * @param Secteur $secteur
     * @return void
     */
    public static function store(Secteur $secteur){
        try{
            $pdo = new TPPDO();
            $statement = $pdo->prepare("INSERT INTO secteur(LIBELLE) values (:libelle)");
            $statement->execute(["libelle" => $secteur->getLibelle()]);
        }
        catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de l'enregistrement d'un Secteur : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }
    }

    /**
    * Supprime le secteur ayant l'id passé en paramètre
    * @param int $id
    */
    public static function delete(int $id){
        try{
            $pdo = new TPPDO();

            // Suppression des liaisons
            $statement = $pdo->prepare("DELETE FROM secteurs_structures WHERE id_secteur = :id");
            $statement->execute(["id"=> $id]);

            $statement = $pdo->prepare("DELETE FROM secteur WHERE id = :id");
            $statement->execute(["id"=> $id]);
        }
        catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de la suppression d'un Secteur : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }
    }
}
