<?php

namespace LP\DAO;

require_once(__DIR__.'/../modeles/Structure.php');
use LP\Modeles\Structure;

require_once(__DIR__.'/../modeles/Util.php');
use LP\Modeles\Util;

require_once(__DIR__.'/TPPDO.php');
use LP\Database\TPPDO;



class StructureDAO {

    /**
     * Renvoie toutes les structures présentes dans la BDD
     * @return Array<Structure>
     */
    public static function all() {
        try {
            $pdo = new TPPDO();
            $statement = $pdo->prepare("SELECT * FROM structure");
            // $statement->setFetchMode(TPPDO::FETCH_CLASS, self::class);
            $statement->execute();
            $structures_db = $statement->fetchAll();

            $structure_instances = [];
            foreach ($structures_db as $structure_db) {
                $gens = $structure_db['ESTASSO'] ? $structure_db['NB_DONATEURS'] :  $structure_db['NB_ACTIONNAIRES'];
                $structure_instances[] = Util::buildStructure($structure_db['NOM'], $structure_db['RUE'], $structure_db['CP'], $structure_db['VILLE'], $gens, $structure_db['ESTASSO']==1, (int) $structure_db['ID']);
            }

            return $structure_instances;

        } catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de la récupération des Structures : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }
        return null;
    }

    /**
     * Renvoie la structure dont l'id est passé en paramètre, ou bien null
     * @return Structure/null
     */
    public static function withId(int $id) {
        try {
            $pdo = new TPPDO();
            $statement = $pdo->prepare("SELECT * FROM structure WHERE id = :id");
            //$statement->setFetchMode(TPPDO::FETCH_CLASS, self::class);
            $statement->execute(["id" => $id]);
            $structure_db = $statement->fetch();
            $gens = $structure_db['ESTASSO'] ? $structure_db['NB_DONATEURS'] :  $structure_db['NB_ACTIONNAIRES'];
            $structure_instance = Util::buildStructure($structure_db['NOM'], $structure_db['RUE'], $structure_db['CP'], $structure_db['VILLE'], $gens, $structure_db['ESTASSO'], (int) $structure_db['ID']);

            return $structure_instance;
        } catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de la récupération d'une Structure : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }
        return null;
    }

    /**
     * Enregistre la structure dans la BDD
     * @param Structure $structure
     */
    public static function store(Structure $structure) {
        try {
            $pdo = new TPPDO();
            $statement = $pdo->prepare("INSERT INTO structure(NOM, RUE, CP, VILLE, ESTASSO, NB_DONATEURS, NB_ACTIONNAIRES) values (:nom, :rue, :cp, :ville, :is_asso, :nb_dona, :nb_actio)");
            $statement->execute(["nom" => $structure->getNom(),
                                "rue" => $structure->getRue(),
                                "cp" => $structure->getCodePostal(),
                                "ville" => $structure->getVille(),
                                "is_asso" => $structure->isAsso(),
                                "nb_dona" => $structure->isAsso() ? $structure->getNbDonateurs() : null,
                                "nb_actio" => $structure->isAsso() ? null :  $structure->getNbActionnaires()]);
        } catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de l'enregistrement d'une Structure : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }
    }

    /**
    * Met à jour une structure dans la BDD
    * @param Structure $structure
    */
    public static function update(Structure $structure) {
        try {
            $pdo = new TPPDO();
            $statement = $pdo->prepare("UPDATE structure
                                        SET NOM = :nom, RUE = :rue, CP = :cp, VILLE = :ville, ESTASSO = :is_asso, NB_DONATEURS = :nb_dona, NB_ACTIONNAIRES = :nb_actio
                                        WHERE ID = :id");
            $statement->execute(["id" => $structure->getId(),
                                "nom" => $structure->getNom(),
                                "rue" => $structure->getRue(),
                                "cp" => $structure->getCodePostal(),
                                "ville" => $structure->getVille(),
                                "is_asso" => $structure->isAsso(),
                                "nb_dona" => $structure->isAsso() ? $structure->getNbDonateurs() : null,
                                "nb_actio" => $structure->isAsso() ? null :  $structure->getNbActionnaires()]);
        } catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de la mise à jour d'une Structure : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }
    }

    /**
    * Supprime une structure de la bdd
    * @param int $id
    */
    public static function delete(int $id) {
        try {
            $pdo = new TPPDO();
            //Suppression des liaisons
            $statement = $pdo->prepare("DELETE FROM secteurs_structures WHERE id_structure = :id");
            $statement->execute(["id" => $id]);

            $statement = $pdo->prepare("DELETE FROM structure WHERE ID = :id");
            $statement->execute(["id" => $id]);
        } catch (PDOException $e) {
            echo "Erreur ".$e->getCode()." lors de la suppression d'une Structure : ".$e->getMessage()."<br/>".$e->getTraceAsString();
        }
    }
}
